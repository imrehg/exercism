def reverse(text: str) -> str:
    """Reverses the input string."""
    return text[::-1]
